import * as React from 'react';
import { Text, TouchableOpacity, StyleSheet, Animated } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

interface TextStyle {
  fontFamily?: string;
  fontSize?: number;
  color?: string;
}

interface Props {
  checked: boolean;
  bgChecked?: string;
  bgUnChecked?: string;
  bdUnChecked?: string;
  iconSize?: number;
  text?: string;
  textStyle?: TextStyle;
  onPress: Function;
}

const styles = StyleSheet.create({
  container: {
    padding: 8,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  boxContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 6,
  },
  text: {
    marginLeft: 8,
    fontSize: 16,
  },
});

const AnimatedCheckbox: React.FunctionComponent<Props> = ({
  checked,
  bgChecked,
  bgUnChecked,
  bdUnChecked,
  iconSize,
  text,
  textStyle,
  onPress,
}: Props) => {
  const [scale] = React.useState(new Animated.Value(1));

  const handlePress = () => {
    scale.setValue(0.7);

    Animated.spring(scale, {
      toValue: 1,
      friction: 3,
    }).start();

    onPress();
  };

  return (
    <TouchableOpacity
      activeOpacity={0.6}
      style={styles.container}
      onPress={handlePress}>
      <Animated.View
        style={{
          ...styles.boxContainer,
          backgroundColor: checked ? bgChecked : bgUnChecked,
          borderColor: checked ? bgChecked : bdUnChecked,
          transform: [{ scale }],
        }}>
        <Icon name="check" size={iconSize} color="#FFFFFF" />
      </Animated.View>
      <Text style={{ ...styles.text, ...textStyle }}>{text}</Text>
    </TouchableOpacity>
  );
};

AnimatedCheckbox.defaultProps = {
  checked: false,
  bgChecked: '#0154C6',
  bdUnChecked: '#979797',
  bgUnChecked: '#FFFFFF',
  iconSize: 20,
  text: 'Example text',
  textStyle: {},
} as Partial<Props>;

export default AnimatedCheckbox;
